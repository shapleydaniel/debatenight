﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards
{

    public class CardHolder : MonoBehaviour, IDropHandler
    {
        [SerializeField] protected Transform parent;
        
        public virtual void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) return;
            var card = eventData.pointerDrag.GetComponent<Card>();
            card.SetParent(parent);
        }
    }
}
