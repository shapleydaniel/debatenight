﻿using System;
using CardTypes;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards
{
    [RequireComponent(typeof(CanvasGroup))]
    public class Card : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [Header("Tween")] 
        [SerializeField] private RectTransform cardTrans;
        [SerializeField] private Ease ease;
        [NonSerialized] public DebateCardData cardData;
        
        private Vector2 animationPosition;
        private Transform previousParent;
        private int previousChildIndex;
        private RectTransform rectTrans;
        private CanvasGroup canvasGroup;
        private Canvas canvas;
        
        public Vector2 AnimationPosition
        {
            set => animationPosition = value;
        }
        
        public virtual void OnEnable()
        {
            var position = GetComponent<RectTransform>().anchoredPosition;
            cardTrans.anchoredPosition = position + animationPosition;
            //TEMP
            DrawCardAnimation(0.5f);
        }

        private void Awake()
        {
            rectTrans = GetComponent<RectTransform>();
            canvasGroup = GetComponent<CanvasGroup>();
            var parent = transform.parent;
            previousParent = parent;
            previousChildIndex = transform.GetSiblingIndex();
            canvas = (Canvas)FindObjectOfType(typeof(Canvas));
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            canvasGroup.alpha = 0.6f;
            canvasGroup.blocksRaycasts = false;
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            rectTrans.anchoredPosition += eventData.delta/ canvas.scaleFactor;
            transform.SetParent(canvas.transform);
        }
    
        public virtual void OnEndDrag(PointerEventData eventData)
        {
            transform.parent = previousParent;
            transform.SetSiblingIndex(previousChildIndex);
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts =true;
        }

        public void ResetPosition()
        {
            transform.parent = previousParent;
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts =true;
        }

        public void SetCardData(DebateCardData newCardData)
        {
            cardData = newCardData;
        }

        public Tween DrawCardAnimation(float duration)
        {
            return cardTrans.DOAnchorPosX(0, duration).SetEase(ease);
        }

        public void ResetAnimation()
        {
            cardTrans.anchoredPosition = animationPosition;
        }

        public void Selected()
        {
            var cardPosition = cardTrans.anchoredPosition;
            cardPosition.y += 50;
            cardTrans.anchoredPosition = cardPosition;
        }

        public void Deselected()
        {
            var cardPosition = cardTrans.anchoredPosition;
            cardPosition.y -= 50;
            cardTrans.anchoredPosition = cardPosition;
        }

        public virtual void DeactivateCard()
        {
            
        }

        public virtual void SetCardDetails(){}

        public void SetParent(Transform newParent)
        {
            transform.SetParent(newParent);
            previousParent = transform.parent;
        }
    }
}
