﻿using System;
using UnityEngine;

namespace CardTypes
{
    public class CardContainer
    {
        public object item;

        public void SetItem<T>(T value)
        {
            item = value;
        }
    }

    [Serializable]
    public class DebateCardData
    {
        public DebateCardType cardType;
        public float value;
        public float time;
        public Color color;
        public PlayStyle cardStyle;
        public int weight;
        public string animation;

        public int[] inputValues;
        public int[] outputValues;
    }

    public enum PlayStyle
    {
        Offensive,
        Defensive
    }

    public enum DebateCardType
    {
        None,
        Patience,
        Time,
        Audience,
        SkipTurn,
        AllowTurn
    }

    public struct PlayerData
    {
        public string title;
        public string name;
        public float patience;
        public float audience;
        public float time;
        public bool turnAllowed;
        public int playerNumber;
    }
}