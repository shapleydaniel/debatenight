﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cards;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

public class Hand : MonoBehaviour, IDropHandler
{
    [Header("Generic Hand")]
    [SerializeField] private Transform cardPosition;
    [SerializeField] private int maxHand;
    [SerializeField] private AssetReference cardPrefab;
    [SerializeField] private float drawAnimationDuration;
    
    public List<DebateCardData> deck = new List<DebateCardData>();

    private readonly List<GameObject> cardPool = new List<GameObject>();
    private readonly List<Card> cards = new List<Card>();
    private readonly List<int> animatedCards = new List<int>();
    private int activeCards;

    private int selectedCard;
    
    public static Action reduceActiveCards;

    protected virtual void OnEnable()
    {
        reduceActiveCards += ()=> activeCards--;
        
        if(!cardPrefab.RuntimeKeyIsValid())
        {
            Debug.Log("Invalid key" + cardPrefab.RuntimeKey);
            return;
        }
        
        Addressables.LoadAssetAsync<GameObject>(cardPrefab);
        Addressables.LoadAssetAsync<GameObject>(cardPrefab).Completed += SetPool;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            AdjustSelectedCard(-1);
            Debug.Log(selectedCard);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            AdjustSelectedCard(1);
            Debug.Log(selectedCard);
        }
    }

    public void SetDeck(DebateDeck newDeck)
    {
        deck = newDeck.deck;
    }

    private void AdjustSelectedCard(int value)
    {
        DeselectCard();
        
        if (selectedCard + value > 0 && selectedCard + value < activeCards)
        {
            selectedCard += value;
        } else if (selectedCard+value < 0)
        {
            selectedCard = activeCards;
        }
        else
        {
            selectedCard = 0;
        }
        
        SelectCard();
    }

    private void SelectCard()
    {
        cards[selectedCard].Selected();
    }

    private void DeselectCard()
    {
        cards[selectedCard].Deselected();
    }

    private void SetPool(AsyncOperationHandle<GameObject> prefab)
    {
        for (var i = 0; i < maxHand; i++)
        {
            CreateCard();
            activeCards = 0;
        }
    }

    public void DrawCards()
    {
        var numberCards = maxHand - activeCards;

        for (var i = 0; i < numberCards; i++)
        {
            var currentCardData = deck[Random.Range(0, deck.Count)];
            
            if (!CheckCards(currentCardData))
            {
                CreateCard();
            }
        }
        
        DrawCardsAnimations();
    }

    private bool CheckCards(DebateCardData cardData)
    {
        for (var i = 0; i < cardPool.Count; i++)
        {
            var card = cardPool[i];
            if (card.activeSelf) continue;
            animatedCards.Add(i);
            var cardScript = card.GetComponent<Card>();
            cardScript.SetCardData(cardData);
            card.SetActive(true);
            cardScript.SetCardDetails();
            activeCards++;
            return true;
        }
        
        return false;
    }
    
    private void CreateCard()
    {
        var cardObject = Addressables.InstantiateAsync(cardPrefab, cardPosition).Result;
        var cardScript = cardObject.GetComponent<Card>();
        cards.Add(cardScript);
        cardScript.AnimationPosition = new Vector2(-transform.position.x*3, 0);
        cardPool.Add(cardObject);
        cardObject.SetActive(false);
    }

    public void SetHandTo(bool active)
    {
        foreach (var card in cardPool.Where(card => card.activeSelf))
        {
            card.GetComponent<CanvasGroup>().blocksRaycasts = active;
        }
    }

    private void DrawCardsAnimations()
    {
        var sequence = DOTween.Sequence();
        sequence.Pause();

        foreach (var card in animatedCards.Select(cardNumber => cards[cardNumber]))
        {
            card.ResetAnimation();
            sequence.Append(card.DrawCardAnimation(drawAnimationDuration));
        }
        
        animatedCards.Clear();

        sequence.Play();
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;
        var card = eventData.pointerDrag.GetComponent<DebateCard>();
        card.SetParent(transform);
    }
}
