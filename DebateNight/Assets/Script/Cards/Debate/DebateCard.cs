﻿using Cards;
using CardTypes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DebateCard : Card
{
     [Header("Combo")]
     [SerializeField] private TextMeshProUGUI inputText;
     [SerializeField] private TextMeshProUGUI outputText;
     private int comboPosition = -1;

     [Header("Additional Assets")]
     [SerializeField] private Image background;

     private DebateCardData debateCardData;

     public DebateCardData DebateCardData => debateCardData;

     public int ComboPosition
     {
          set => comboPosition = value;
     }

     public void SetCardDetails(DebateCardData cardData)
     {
          debateCardData = cardData;
          background.color = debateCardData.color;
          inputText.text = debateCardData.inputValues[0].ToString("00");
          outputText.text = debateCardData.outputValues[0].ToString("00");
     }

     public override void DeactivateCard()
     {
          debateCardData = new DebateCardData();
          ResetPosition();
          Hand.reduceActiveCards?.Invoke();
          gameObject.SetActive(false);
     }
}
