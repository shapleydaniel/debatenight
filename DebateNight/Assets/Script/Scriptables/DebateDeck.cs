﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Deck", fileName = "Deck")]
public class DebateDeck : ScriptableObject
{
    public List<DebateCardData> deck = new List<DebateCardData>();
}
