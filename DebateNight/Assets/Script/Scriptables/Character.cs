﻿using UnityEngine;

[CreateAssetMenu(menuName = "Character/Character", fileName = "Character")]
public class Character : ScriptableObject
{
    [Header("UI Data")]
    public string name;
    public Sprite menuCharacter; 
    public Sprite opponentCharacter;
    public Sprite playerCharacter;

    [Header("Game Data")] 
    public bool aiOn;
    public DebateDeck deck;
    public AIStyle aiStyle;
    public int playerNumber = -1;
    public CharacterStats stats;
}
