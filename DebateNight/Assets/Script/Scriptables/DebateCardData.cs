﻿using CardTypes;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Card", fileName = "Card")]
public class DebateCardData : ScriptableObject
{
    public DebateCardType cardType;
    public float value;
    public float time;
    public Color color;
    public PlayStyle cardStyle;
    public int weight;
    public string animation;

    public int[] inputValues;
    public int[] outputValues;
}
