using UnityEngine;

[CreateAssetMenu(menuName = "Character/Stats", fileName = "Stats")]
public class CharacterStats : ScriptableObject
{
    public float startingAudience;
    public float startingTime;
    public float startingPatience;

    public bool allowedTurn;
    public float audience;
    public float time;
    public float patience;
}
