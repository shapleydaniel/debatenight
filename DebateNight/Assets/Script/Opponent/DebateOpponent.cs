﻿using System.Collections;
using System.Collections.Generic;
using CardTypes;
using Script.Cards.Debate.Players;
using UnityEngine;
using Random = UnityEngine.Random;

public struct PriorityValues
{
    public DebateCardType type;
    public float value;
}

public class DebateOpponent : BasePlayer
{
    public List<DebateCardData> hand = new List<DebateCardData>();
    public List<DebateCardData> selectedCards = new List<DebateCardData>();
    public List<DebateCardData> deck = new List<DebateCardData>();

    private DebateCardData selectedCard;
    private int currentState;
    private PlayStyle currentPlayStyle;
    private PriorityValues currentPriority;
    private int selectedPlayer;

    private void Start()
    {
        //TODO REMOVE ONCE OPPONENT can select
        SetCharacterImage();
    }

    public void TurnOrder()
    {
        switch (currentState)
        {
            case 0:
                DrawCards();
                break;
            case 1:
                SelectCard();
                break;
            case 2:
                StartCoroutine(PlayCard());
                break;
            case 3:
                StartCoroutine(EndTurn());
                break;
        }
    }

    private void DrawCards()
    {
        while (hand.Count < 5)
        {
            DrawCard();
        }

        currentState++;
    }
    
    private void DrawCard()
    {
        var card = deck[Random.Range(0, deck.Count)];
        hand.Add(card);
    }

    private void SelectCard()
    {
        GiveWeightToCards();
        if (selectedCards.Count > 0)
        {
            selectedCards.Sort((s1,s2) => s1.weight.CompareTo(s2.weight));
            selectedCard = selectedCards[0];
            currentState++;
            TurnOrder();
        }
        else
        {
            currentState = 3;
            TurnOrder();
        }
       
    }

    private void GiveWeightToCards()
    {
        CheckStat();
        DecidePlayStyle();

        foreach (var t in hand)
        {
            var card = t;
            if (currentTime - card.time > 0)
            {
                var weight = (int) card.value * 10;
                if (currentPlayStyle == card.cardStyle)
                {
                    weight++;
                
                    if (currentPriority.type == card.cardType)
                    {
                        weight++;
                    }
                }

                card.weight = weight;
                selectedCards.Add(card);
            }
        }
    }
    
    private void CheckStat()
    {
        var health = new PriorityValues{type =  DebateCardType.Patience, value = characterData.stats.patience};
        var audience = new PriorityValues{type =  DebateCardType.Audience, value = characterData.stats.audience};
        var time = new PriorityValues{type =  DebateCardType.Time, value = characterData.stats.time};
        var order = new List<PriorityValues>{health, audience, time};
        order.Sort((s1, s2) => s1.value.CompareTo(s2.value));
        Debug.Log("Priority: " + order[0].type);
        currentPriority = order[0];
    }
    
    private void DecidePlayStyle()
    {
        currentPlayStyle = currentPriority.value < 0.5f ? PlayStyle.Defensive : PlayStyle.Offensive;
        Debug.Log("Current Play style: " + currentPlayStyle);
    }

    private int StrongestOpponent()
    {
        return -1;
    }
    
    private IEnumerator PlayCard()
    {
        selectedPlayer = selectedCard.cardStyle == PlayStyle.Defensive ? characterData.playerNumber : StrongestOpponent();
        
        opponentPlayedCard?.Invoke(selectedPlayer, selectedCard);
        ReduceTime(selectedCard.time);
        
        hand.Remove(selectedCard);
        selectedCards.Clear();
        selectedCard = new DebateCardData();
        
        yield return new WaitForSeconds(2f);
        currentState--;
        TurnOrder();
    }

    //REMOVE IF not by number of cards
    private IEnumerator EndTurn()
    {
        yield return new WaitForSeconds(2f);
        currentState = 0;
        ResetTime();
        OpponentController.opponentsTurnEnded?.Invoke();
    }
    
}
