﻿using System;
using System.Collections.Generic;
using CardTypes;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class OpponentController : MonoBehaviour
{
    [Header("Assets")]
    [SerializeField] private AssetReference opponentPrefab;
    [SerializeField] private int numberOpponents;

    private List<DebateOpponent> opponents = new List<DebateOpponent>();
    private int currentOpponent;

    public static Action opponentsTurnEnded;

    private void OnEnable()
    {
        opponentsTurnEnded += OpponentsTurnEnded;
    }

    private void OnDisable()
    {
        opponentsTurnEnded = null;
    }

    private void Awake()
    {
        Addressables.LoadAssetAsync<GameObject>(opponentPrefab);
        Addressables.LoadAssetAsync<GameObject>(opponentPrefab).Completed += CreateOpponents;
    }

    private void CreateOpponents(AsyncOperationHandle<GameObject> opponentAddressable)
    {
        for (var i = 0; i < numberOpponents; i++)
        {
            var opponent = Addressables.InstantiateAsync(opponentPrefab, transform).Result;
            var opponentData = opponent.GetComponent<DebateOpponent>();
            opponents.Add(opponentData);
            //opponentData.SetAllBars();
        }
    }

    public void SetupOpponentsTurn()
    {
        DebateStateManager.currentPlayersTurn = currentOpponent;
        var opponent = opponents[currentOpponent];
        if (opponent == null) return;
        opponent.TurnOrder();
    }
    
    public void OpponentsTurn()
    {
        var opponent = opponents[currentOpponent];
        if (opponent == null) return;
        opponent.TurnOrder();
    }
    
    private void OpponentsTurnEnded()
    {
        currentOpponent++;
        if (currentOpponent >= opponents.Count)
        {
            currentOpponent = 0;
            DebateStateManager.opponentsTurnEnded?.Invoke();
        }
        else
        {
            opponents[currentOpponent].TurnOrder();
        }
    }
}
