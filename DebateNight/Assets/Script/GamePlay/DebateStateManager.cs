﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Script.Cards.Debate.Players;
using TMPro;
using UnityEngine;

public enum TurnState
{
    Start,
    PlayersTurn,
    OpponentsTurn,
    EndTurns
}

public class DebateStateManager : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private OpponentController opponentController;
    [SerializeField] private List<TurnState> orderOfPlay;
    [SerializeField] private TurnDisplay turnDisplay;
    
    private int turnCount = 1;
    
    private TurnState currentState = TurnState.Start;
    private int currentIntState;

    public static int currentPlayersTurn;
    
    public static Action opponentsTurnEnded;
    public static Action playersTurnEnded;

    private void OnEnable()
    {
        opponentsTurnEnded += OpponentsTurnEnded;
        playersTurnEnded += PlayerTurnEnded;
        currentState = TurnState.Start;
        turnDisplay.DisplayTurn(turnCount);
    }

    private void OnDisable()
    {
        opponentsTurnEnded = null;
        playersTurnEnded = null;
    }

    private void Start()
    {
        TurnStateAltered();
    }

    private void TurnStateAltered()
    {
        switch (currentState)
        {
            case TurnState.Start:
                StartCoroutine(SetupDebate());
                break;
            case TurnState.PlayersTurn:
                currentPlayersTurn = -1;
                turnDisplay.TabIntro(currentState, ()=>{ player.StartPlayersTurn(); }, 
                    () => {player.PlayersTurn();});
                break;
            case TurnState.OpponentsTurn:
                turnDisplay.TabIntro(currentState, () => { opponentController.SetupOpponentsTurn(); }, 
                    () => { opponentController.OpponentsTurn(); });
                break;
            case TurnState.EndTurns:
                turnCount++;
                if (turnCount > Constants.Game.MaxTurns)
                {
                    EndGame();
                }
                else
                {
                    currentIntState = 0;
                    currentState = orderOfPlay[0];
                    turnDisplay.DisplayTurn(turnCount);
                    TurnStateAltered();
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private IEnumerator SetupDebate()
    {
        yield return new WaitForSeconds(2f);
        currentState = orderOfPlay[currentIntState];
        TurnStateAltered();
    }

    private void OpponentsTurnEnded()
    {
        NextPlayer();
        TurnStateAltered();
    }
    
    private void PlayerTurnEnded()
    {
        NextPlayer();
        TurnStateAltered();
    }

    private void EndGame()
    {
        
    }
    
    private void NextPlayer()
    {
        currentIntState++;
        currentState = currentIntState >= orderOfPlay.Count ? TurnState.EndTurns : orderOfPlay[currentIntState];
    }
}
