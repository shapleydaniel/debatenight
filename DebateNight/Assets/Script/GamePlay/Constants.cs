﻿

namespace Constants
{
    public enum Stats
    {
        Patience,
        Time,
        Audience
    }
   
    public class Game
    {
        public const int MaxTurns = 5;
    }

    public class Animations
    {
        public const string Slash = "SlashAnimation";
    }
}
