﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class TurnDisplay : MonoBehaviour
{
    //Might have Separate Script
    [Header("Turn Tab")]
    [SerializeField] private TextMeshProUGUI turnsTextMesh;
    [SerializeField] private TextMeshProUGUI turnsTextShadow;
    [SerializeField] private RectTransform tabRectTrans;
    [SerializeField] private float tabDuration;
    [SerializeField] private Ease tabEase;

    [Header("Turn Display")] 
    [SerializeField] private TextMeshProUGUI turnDisplayMesh;
    [SerializeField] private TextMeshProUGUI turnDisplayMeshShadow;
    
    private Tween tabTween;
    private Sequence tabSequence;
    private int currentTweenState;
    
    private Action onTweenMidPoint;
    private Action onTweenComplete;
    
    private void OnEnable()
    {
        CreateTabAnimation();
    }

    private void CreateTabAnimation()
    {
        tabSequence = DOTween.Sequence();
        tabSequence.SetAutoKill(false);
        
        var position = tabRectTrans.anchoredPosition.x;
        var tween = tabRectTrans.DOAnchorPosX(-100, tabDuration).SetEase(tabEase);
        tween.onComplete += TweenStage;
        tabSequence.Append(tween);
        tabSequence.AppendInterval(2f);

        tween = tabRectTrans.DOAnchorPosX(position, tabDuration).SetEase(Ease.InCubic);
        tween.onComplete += TweenStage;
        tabSequence.Append(tween);
        tabSequence.Pause();
    }
    
    private void TweenStage()
    {
        currentTweenState++;
        if (currentTweenState == 1)
        {
            onTweenMidPoint?.Invoke();
        }
    }
    
    public void TabIntro(TurnState currentState ,Action animationMidPoint, Action animationComplete)
    {
        CheckCurrentStateText(currentState);
        
        onTweenMidPoint += animationMidPoint;
        tabSequence.onComplete += () =>
        {
            animationComplete?.Invoke();
            currentTweenState = 0;
            onTweenMidPoint = null;
            tabSequence.onComplete = null;
        };
        
        tabSequence.Restart();
    }

    private void CheckCurrentStateText(TurnState currentState)
    {
        string text;
        switch (currentState)
        {
            case TurnState.Start:
                break;
            case TurnState.PlayersTurn:
                text = "PLAYERS TURN";
                turnsTextMesh.text = text;
                turnsTextShadow.text = text;
                break;
            case TurnState.OpponentsTurn:
                text = "OPPONENTS TURN";
                turnsTextMesh.text = text;
                turnsTextShadow.text = text;
                break;
            case TurnState.EndTurns:
                text = "DEBATE END";
                turnsTextMesh.text = text;
                turnsTextShadow.text = text;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void DisplayTurn(int turnCount)
    {
        turnDisplayMesh.text = turnCount.ToString("00");
        turnDisplayMeshShadow.text = turnCount.ToString("00");
    }
}
