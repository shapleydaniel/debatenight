﻿using Cards;
using UnityEngine;

namespace Script.Cards.Debate.Players
{
    public class Player : BasePlayer
    {
        
        [SerializeField] private GameObject comboHolder;
        
        private void Start()
        {
            SetCharacterImage();
        }

        public void StartPlayersTurn()
        {
            hand.DrawCards();
        }

        public void PlayersTurn()
        {
            comboHolder.SetActive(true);
            hand.SetHandTo(true);
        }
    

        public void EndTurn()
        {
            ResetTime();
            hand.SetHandTo(false);
            DebateStateManager.playersTurnEnded?.Invoke();
        }

        private void CheckCardCanPlay(int selectedPlayer, Card currentCard)
        {
            var cardData = currentCard.cardData;
            if (currentTime - cardData.time < 0)
            {
                currentCard.ResetPosition();
            }
            else
            {
                ReduceTime(cardData.time);
                playCard?.Invoke(selectedPlayer, currentCard);
            }
        }
    }
}
