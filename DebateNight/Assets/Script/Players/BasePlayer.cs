﻿using System;
using Cards;
using CardTypes;
using Constants;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Cards.Debate.Players
{
    public class BasePlayer : MonoBehaviour
    {
        [SerializeField] protected Character characterData;
        [SerializeField] private StatsController stats;
        [SerializeField] private Image characterImage;
        [SerializeField] protected Hand hand;

        public float currentTime;

        protected static Action<int, DebateCardData> opponentPlayedCard;
        protected static Action<int, Card> playCard;

        protected virtual void OnEnable()
        {
            opponentPlayedCard += OpponentCard;
            playCard += PlayCard;
            SetAllBars();
            hand.SetDeck(characterData.deck);
        }

        private void OnDisable()
        {
            opponentPlayedCard = null;
            playCard = null;
        }

        public void SetCharacter(Character character)
        {
            characterData = character;
            hand.SetDeck(character.deck);
            SetAllBars();
        }

        public void SetCharacterImage()
        {
            if (characterData == null) return;
            characterImage.sprite = characterData.aiOn ? characterData.opponentCharacter : characterData.playerCharacter;
        }

        private void CheckEffect(DebateCardData card)
        {
            switch (card.cardType)
            {
                case DebateCardType.Patience:
                    if (CheckValue(characterData.stats.patience, card.cardStyle))
                    {
                        var patience = characterData.stats.patience;
                        patience += card.value;
                        patience = Mathf.Clamp(patience, 0f, 1f);
                        stats.TweenBar(Constants.Stats.Patience, patience);
                    }
                    break;
                case DebateCardType.Time:
                    if (CheckValue(characterData.stats.time, card.cardStyle))
                    {
                        currentTime += Mathf.Clamp(card.value, 0f, 1f);
                        stats.TweenBar(Stats.Time, currentTime);
                    }
                    break;
                case DebateCardType.Audience:
                    if (CheckValue(characterData.stats.audience,card.cardStyle))
                    {
                        var audience = characterData.stats.audience;
                        audience += card.value;
                        audience = Mathf.Clamp(audience, 0f, 1f);
                        stats.TweenBar(Stats.Audience, audience);
                        if(card.animation != null) AnimationController.playAnimation?.Invoke(card.animation, new Vector2(2,2));
                    }
                    break;
                case DebateCardType.SkipTurn:
                    characterData.stats.allowedTurn = false;
                    break;
                case DebateCardType.AllowTurn:
                    characterData.stats.allowedTurn = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(card.cardType), card.cardType, null);
            }
        }

        public void SetAllBars()
        {
            currentTime = characterData.stats.time;
            stats.TweenBar(Stats.Audience, characterData.stats.audience);
            stats.TweenBar(Stats.Time, currentTime);
            stats.TweenBar(Stats.Patience, characterData.stats.patience);
        }

        private bool CheckValue(float value, PlayStyle style)
        {
            if (style != PlayStyle.Defensive) return value > 0;
            return DebateStateManager.currentPlayersTurn == characterData.playerNumber && value < 1;
        }

        protected void ResetTime()
        {
            currentTime += characterData.stats.time;
            currentTime = Mathf.Clamp(currentTime, 0, 1);
            stats.AdjustBar(Stats.Time, currentTime);
            Debug.Log($"Current Time: {currentTime}");
        }

        protected void ReduceTime(float value)
        {
            currentTime -= value;
            stats.AdjustBar(Stats.Time, currentTime);
        }
    
        private void PlayCard(int selectedPlayer, Card card)
        {
            //NEED TO FIGURE OUT SPECIFIC DATA
            var cardData = card.cardData;
            switch (cardData.cardStyle)
            {
                case PlayStyle.Defensive when selectedPlayer == DebateStateManager.currentPlayersTurn:
                    CheckEffect(cardData);
                    card.DeactivateCard();
                    break;
                case PlayStyle.Offensive when selectedPlayer == characterData.playerNumber:
                    CheckEffect(card.cardData);
                    card.DeactivateCard();
                    break;
                default:
                    card.ResetPosition();
                    break;
            }
        }

        private void OpponentCard(int playerNumber, DebateCardData card)
        {
            if (characterData.playerNumber != playerNumber) return;
            CheckEffect(card);
        }
    }
}
