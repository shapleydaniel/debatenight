﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AnimationController : MonoBehaviour
{
   [SerializeField] private List<AssetReference> animations = new List<AssetReference>();
   
   private AssetReference selectedAnimationPrefab;
   private Vector2 selectedAnimationPosition;
   private readonly List<GameObject> animationPool =  new List<GameObject>();

   public static Action<string, Vector2> playAnimation;
   public static bool AnimationPlaying = false;
   
   private void OnEnable()
   {
      playAnimation += PlayAnimation;
   }

   private void LoadAnimations()
   {
      //TODO: Load all available animations
   }

   private void PlayAnimation(string animationName, Vector2 animationPosition)
   {
      //Check for animation
      AnimationPlaying = true;
      selectedAnimationPosition = animationPosition;
      var animationObject = CheckForAnimationPool(animationName);
      if (animationObject == null)
      {
         CreateAnimation(animationName);
      }
      else
      {
         animationObject.transform.position = animationPosition;
         animationObject.SetActive(true);
      }
   }

   private GameObject CheckForAnimationPool(string animationName)
   {
      return animationPool.Count == 0 ? null : animationPool.FirstOrDefault(animationObject => animationObject.name == animationName);
   }

   private void CreateAnimation(string animationName)
   {
      selectedAnimationPrefab = FindAnimationPrefab(animationName);
      if(selectedAnimationPrefab == null){ Debug.Log("Error No Animation in List");
         AnimationPlaying = false; return;}
      if(!selectedAnimationPrefab.RuntimeKeyIsValid())
      {
         Debug.Log("Invalid key" + selectedAnimationPrefab.RuntimeKey);
         return;
      }
      Addressables.LoadAssetAsync<GameObject>(selectedAnimationPrefab);
      Addressables.LoadAssetAsync<GameObject>(selectedAnimationPrefab).Completed += CreateAnimation;
   }

   private void CreateAnimation(AsyncOperationHandle<GameObject> obj)
   {
      var animationObject = Addressables.InstantiateAsync(selectedAnimationPrefab, transform).Result;
      animationPool.Add(animationObject);
      animationObject.transform.position = selectedAnimationPosition;
   }

   private AssetReference FindAnimationPrefab(string animationName)
   {
      //return animations.FirstOrDefault(animationAsset =>  animationAsset.Asset.name == animationName);
      //TEMP till they fix the issue with addressable searching;
      return animations[0];
   }
}
