﻿using UnityEngine;

public class AnimationObject : MonoBehaviour
{
   public void DeactivateAnimation()
   {
      gameObject.SetActive(false);
      AnimationController.AnimationPlaying = false;
   }
}
