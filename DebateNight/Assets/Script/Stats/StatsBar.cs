﻿using DG.Tweening;
using UnityEngine;

public class StatsBar : MonoBehaviour
{
   [SerializeField] private RectTransform bar;
   
   [Header("Tween")] 
   [SerializeField] private float duration;
   [SerializeField] private Ease ease;
   
   private float maxStatsBar;

   public void AdjustStatTween(float stat)
   {
      var currentScale = bar.localScale;
      bar.DOScaleX(stat, duration).SetEase(ease);
   }

   public void AdjustStat(float stat)
   {
      var size = bar.localScale;
      size.x = stat;
      bar.localScale = size;
   }
}
