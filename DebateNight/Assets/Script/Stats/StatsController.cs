﻿using System.Collections.Generic;
using Constants;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    [SerializeField] private List<StatsBar> bars = new List<StatsBar>();

    public void AdjustBar(Constants.Stats stat, float value)
    {
       var bar = bars.Find(x => x.name == stat.ToString());
       bar.AdjustStat(value);
    }

    public void TweenBar(Stats stat, float value)
    {
        var bar = bars.Find(x => x.name == stat.ToString());
        bar.AdjustStatTween(value); 
    }

}
