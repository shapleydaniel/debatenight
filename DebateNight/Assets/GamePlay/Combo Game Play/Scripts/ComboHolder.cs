﻿using System;
using CardTypes;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ComboHolder : MonoBehaviour, IDropHandler
{
    [Header("Assets")]
    [SerializeField]private Transform parent;
    [SerializeField] private GameObject plusIcon;
    [SerializeField] private Image Border;

    [HideInInspector] public DebateCardData card;
    
    public int ComboPosition { get; set; }

    public ComboHolderController Controller
    {
        set => controller = value;
    }

    private bool vacant = true;
    private ComboHolderController controller;

    public static Action<int> HolderVacant;

    private void OnEnable()
    {
        HolderVacant += ResetComboHolder;
    }

    private void OnDisable()
    {
        HolderVacant = null;
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;
        var card = eventData.pointerDrag.GetComponent<DebateCard>();
        if (!controller.CheckCardValid(card.DebateCardData, ComboPosition)) return;
        card.ComboPosition = ComboPosition;
        controller.SetCardValues(card.DebateCardData, ComboPosition);
        ColorBorder(card.DebateCardData.color);
        card.SetParent(parent);
        vacant = false;
    }

    public void SetPlusActive(bool active)
    {
        if(plusIcon != null)plusIcon.SetActive(active);
    }

    private void ColorBorder(Color cardColor)
    {
        Border.color = cardColor;
    }

    private void ResetComboHolder(int CardPosition)
    {
        if (CardPosition != ComboPosition) return;
        vacant = true;
        ColorBorder(Color.white);
        //Remove Card
        controller.RemoveCard(CardPosition);
    }
}
