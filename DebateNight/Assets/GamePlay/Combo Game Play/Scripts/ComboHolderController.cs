﻿using System.Collections.Generic;
using CardTypes;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ComboHolderController : MonoBehaviour
{
    [Header("Assets")]
    [SerializeField] private AssetReference cardHolderPrefab;
    
    [Header("Animation")]
    [SerializeField] private RectTransform BorderTrans;
    [SerializeField] private float duration;
    [SerializeField] private Ease borderEase;

    [Header("Test Assets")]
    [SerializeField] private int maxCombo;
    
    //Combo
    private List<GameObject> holderPool = new List<GameObject>();
    private List<ComboHolder> comboHolders = new List<ComboHolder>();
    private DebateCardData[] cards;

/*    //Combination Combo
    private Dictionary<DebateCardType, int> cardCombo = new Dictionary<DebateCardType, int>();*/
    
    private void OnEnable()
    {
        cards = new DebateCardData[maxCombo];

        if (holderPool.Count >= maxCombo)
        {
            SetAvailableCombo();
        }
        else
        {
            if(!cardHolderPrefab.RuntimeKeyIsValid())
            {
                Debug.Log("Invalid key" + cardHolderPrefab.RuntimeKey);
                return;
            }
        
            Addressables.LoadAssetAsync<GameObject>(cardHolderPrefab);
            Addressables.LoadAssetAsync<GameObject>(cardHolderPrefab).Completed += SetPool;
        }
    }
    
    private void SetPool(AsyncOperationHandle<GameObject> prefab)
    {
        for (var i = 0; i < maxCombo; i++)
        {
            CreateHolder(i);
        }
        
        SetAvailableCombo();
    }
    
    private void CreateHolder(int position)
    {
        var cardObject = Addressables.InstantiateAsync(cardHolderPrefab, transform).Result;
        cardObject.SetActive(false);
        var comboHolder = cardObject.GetComponent<ComboHolder>();
        comboHolder.Controller = this;
        comboHolder.ComboPosition = position;
        comboHolders.Add(comboHolder);
        holderPool.Add(cardObject);
    }

    private void SetAvailableCombo()
    {
        for (var i = 0; i < maxCombo; i++)
        {
            holderPool[i].SetActive(true);
            if(i == maxCombo-1)comboHolders[i].SetPlusActive(false);
        }
    }
    
    public bool CheckCardValid(DebateCardData cardData, int comboPosition)
    {
        var leftCard = GetCard(comboPosition - 1);
        var rightCard = GetCard(comboPosition + 1);

        var left = false;
        var right = false;

        if (null != leftCard) left = leftCard.cardType != DebateCardType.None;
        if(null != rightCard) right = rightCard.cardType != DebateCardType.None;

        //No Cards Added
        if (!left && !right) return true;
        var cardLeft = false;
        var cardRight = false;
            
        //Compare card on left and right
        if(left) cardLeft = cardData.inputValues[0] == leftCard.outputValues[0] && cardData.inputValues[1] == leftCard.outputValues[1];
        if(right) cardRight = cardData.outputValues[0] == rightCard.inputValues[0] && cardData.outputValues[1] == rightCard.inputValues[1];
        
        //First card in order only check outputs
        if (comboPosition == 0)
        {
            //Check if card to the left
            if (rightCard.cardType == DebateCardType.None) return true;
            //Check that left card matches current card;
            return cardData.outputValues[0] == rightCard.inputValues[0] &&
                   cardData.outputValues[1] == rightCard.inputValues[1];
        }

        //Middle of Combo Check both cards;
        if (comboPosition == maxCombo - 1) return !left || cardLeft;
        if (left && right) return cardLeft && cardRight;
        
        //Last in order
        return !left ? cardRight : cardLeft;
    }

    private DebateCardData GetCard(int position)
    {
        if (position < cards.Length && position >= 0) return cards[position];
        return new DebateCardData{cardType = DebateCardType.None};
    }

    public void SetCardValues(DebateCardData card, int comboPosition)
    {
        cards[comboPosition] = card;
    }

    public void RemoveCard(int comboPosition)
    {
        cards[comboPosition] = new DebateCardData();
    }

    public void TriggerCombo()
    {
        if (!CheckCombo()) return;
        CombineComboAnimation();
    }

    private bool CheckCombo()
    {
        //Check middle Cards
        comboHolders.Sort((x1, x2) => x1.ComboPosition.CompareTo(x2.ComboPosition));
        
        //If no card at position 1 return false
        if (comboHolders[0].card.cardType == DebateCardType.None) return false;
        //If no card at position 2 return true;
        if (comboHolders[1].card.cardType != DebateCardType.None)
        {
            //if there is a card at position 2 check position 3;
            return comboHolders[2].card != null;
        }

        return true;
    }
    
    private void CombineComboAnimation()
    {
        //Move all cards into center
        foreach (var holder in holderPool)
        {
            var comboHolder = holder.GetComponent<ComboHolder>();
            
            
            holder.GetComponent<ComboHolder>().SetPlusActive(false);
            var holderRect = holder.GetComponent<RectTransform>();
            //TODO REMOVE MAGIC NUMBER
            holderRect.DOAnchorPosX(Screen.width-170, duration).SetEase(borderEase);
        }

        //TODO REMOVE MAGIC NUMBER
        BorderTrans.DOSizeDelta(new Vector2(-1300, BorderTrans.sizeDelta.y), duration).SetEase(borderEase);
    }

    private void OneCardAnimation()
    {
        
    }

    private void TwoCardAnimation()
    {
        
    }

    private void ThreeCardAnimation()
    {
        
    }

    private void ResetComboHolders()
    {
        foreach (var holder in holderPool)
        {
            
        }
    }
    
}
