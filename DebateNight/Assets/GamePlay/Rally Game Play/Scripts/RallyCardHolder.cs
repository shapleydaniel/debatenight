﻿using System;
using System.Collections.Generic;
using Cards;
using CardTypes;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;

public class RallyCardHolder : CardHolder
{
    [SerializeField] private AssetReference cardPrefab;
    
    private List<DebateCardData> currentRally = new List<DebateCardData>();
    private List<DebateCard> cardPool = new List<DebateCard>();

    private int currentCard;

    public static Action<Card> StartingCard;

    private void OnEnable()
    {
        StartingCard += AddStartingCard;
        
        Addressables.LoadAssetAsync<GameObject>(cardPrefab);
        Addressables.LoadAssetAsync<GameObject>(cardPrefab).Completed += CreateRallyCards;
    }
    
    private void OnDisable()
    {
        StartingCard = null;
    }

    private void CreateRallyCards(AsyncOperationHandle<GameObject> obj)
    {
        for (var i = 0; i < 7; i++)
        {
            CreateCard(new CardContainer().item);
            currentCard = 6;
        }
    }
    
    private void CreateCard<T>(T item)
    {
        var cardObject = Addressables.InstantiateAsync(cardPrefab, parent).Result;
        var debateCard = cardObject.GetComponent<DebateCard>();
        cardPool.Add(debateCard);
        debateCard.DeactivateCard();
    }

    public void AddStartingCard(Card data)
    {
       
    }
    
    public override void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;
        var card = eventData.pointerDrag.GetComponent<Card>();
        var debateCardData = card.cardData;
        if (!CheckCardValid(debateCardData)) return;
        cardPool[currentCard].SetCardDetails(debateCardData);
        card.DeactivateCard();
        currentRally.Add(debateCardData);
        currentCard--;
        cardPool[currentCard].gameObject.SetActive(true);
    }

    private bool CheckCardValid(DebateCardData card)
    {
        return true;
        if (currentRally.Count <= 0) return false;
        return currentRally[0].outputValues[0] == card.inputValues[0];
    }
}
