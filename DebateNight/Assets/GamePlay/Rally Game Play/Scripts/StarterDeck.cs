﻿using System.Collections;
using System.Collections.Generic;
using Cards;
using CardTypes;
using UnityEngine;

public class StarterDeck : MonoBehaviour
{
    [SerializeField] private Card cardPrefab;
    [SerializeField] private List<DebateCardData> deck = new List<DebateCardData>();

    public void StartRound()
    {
        var randomData = deck[Random.Range(0, deck.Count)];
        var card = Instantiate(cardPrefab, transform);
        card.SetCardData(randomData);
        
    }
}
